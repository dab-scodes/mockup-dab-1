<?php
namespace Services;

class ParcellesPoolsDabs {
	protected $dabs = array(
		'3774' => array(
			'3' => 1089
		),
		'3983' => array(
			'2' => 1086
		),
		'5687' => array(
			'8' => '2145',
			'9' => '2389'
		),
		'5697' => array(
			'4' => '4123',
			'6' => '2132',
			'8' => '1047'
		)
	);

	public function __construct() {
		return $this;
	}

	public function getDabs($idParcelle, $idPools) {
		if (!isset($this->dabs[$idParcelle])) {
			return 0;
		}
		if (!isset($this->dabs[$idParcelle][$idPools])) {
			return 0;
		}

		return $this->dabs[$idParcelle][$idPools];
	}
}
