<?php
namespace Models;

class Plqs {
	static function getAll() {
		return [
			(object)[
				'id' => 266118
			],
			(object)[
				'id' => 999612
			],
			(object)[
				'id' => 283570
			],
			(object)[
				'id' => 963246
			],
			(object)[
				'id' => 69459
			],
			(object)[
				'id' => 181478
			],
			(object)[
				'id' => 143737
			],
			(object)[
				'id' => 915815
			],
			(object)[
				'id' => 909603
			]
		];
	}

	static function get($id) {
		$list = self::getAll();
		foreach ($list as $plq) {
			if ($plq->id == $id) {
				return $plq;
			}
		}

		return false;
	}

	static function getSelected() {
		$list = self::getAll();
		foreach ($list as $plq) {
			return $plq;
		}

		return false;
	}
}
