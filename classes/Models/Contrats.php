<?php

namespace Models;

use Models\Pools;
use Models\Parcelles;

class Contrats {
	protected static $contrats = array(
		'3774' => array(
			'3'
		),
		'5687' => array(
			'8'
		),
		'5697' => array(
			'4',
			'6'
		)
	);

	static function getAll() {
		$contrats = array();
		foreach (self::$contrats as $parcelleId => $poolIds) {
			$parcelle = Parcelles::get($parcelleId);
			$contrats[$parcelleId] = (object)[
				'parcelle' => $parcelle,
				'pools' => []
			];
			foreach ($poolIds as $poolId) {
				$pool = Pools::get($poolId);
				$contrats[$parcelleId]->pools[] = $pool;
			}
		}

		return $contrats;
	}
}
