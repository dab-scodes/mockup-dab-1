<?php

namespace Models;

class Parcelles {
	static function getAll() {
		return [
			(object)[
				'surface' => '1433',
				'id' => 3476
			],
			(object)[
				'surface' => '1167',
				'id' => 3774
			],
			(object)[
				'surface' => '1164',
				'id' => 3983
			],
			(object)[
				'surface' => '1358',
				'id' => 4165
			],
			(object)[
				'surface' => '4305',
				'id' => 5687
			],
			(object)[
				'surface' => '1386',
				'id' => 5689
			],
			(object)[
				'surface' => '8752',
				'id' => 5697
			],
			(object)[
				'surface' => '14925',
				'id' => 7085
			],
			(object)[
				'surface' => '4896',
				'id' => 7087
			]
		];
	}

	static function get($id) {
		$list = self::getAll();
		foreach ($list as $pool) {
			if ($pool->id == $id) {
				return $pool;
			}
		}

		return false;
	}
}
