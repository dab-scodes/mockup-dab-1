<?php

namespace Models;

use Models\Pools;
use Models\Parcelles;
use Models\Requerants;
use Models\Contrats;

class Requetes {

	public function __construct() {
		return $this;
	}

	public function getParcelleCurrentConsumption($parcelleId, $poolId) {
		$consumption = 0;
		$list = self::getValidated();
		foreach ($list as $requeteId => $requete) {
			if ($parcelleId != $requete->parcelle->id) {
				continue;
			}
			if ($poolId != $requete->pool->id) {
				continue;
			}
			$consumption += $requete->dab;
		}

		return $consumption;
	}

	static function getAll() {
		return self::getOnGoing()+self::getValidated()+self::getCanceled();
	}

	static function getOnGoing() {
		return self::initRequeteArray(array(
		'2' => (object)[
			'parcelle' => '3774',
			'pool' => '3',
			'requerant' => 1,
			'dab' => 1435
		],
		'14' => (object)[
			'parcelle' => '3774',
			'pool' => '3',
			'requerant' => 5,
			'dab' => 2150
		],
		'15' => (object)[
			'parcelle' => '3774',
			'pool' => '3',
			'requerant' => 6,
			'dab' => 5642
		],
		'8' => (object)[
			'parcelle' => '5687',
			'pool' => '8',
			'requerant' => 1,
			'dab' => 1027
		],
		'11' => (object)[
			'parcelle' => '5687',
			'pool' => '8',
			'requerant' => 2,
			'dab' => 2388
		],
		'13' => (object)[
			'parcelle' => '5697',
			'pool' => '6',
			'requerant' => 3,
			'dab' => 4211
		],
		'13' => (object)[
			'parcelle' => '5697',
			'pool' => '6',
			'requerant' => 4,
			'dab' => 3265
		],
	));
	}

	static function getValidated() {
		return self::initRequeteArray(array(
		'1' => (object)[
			'parcelle' => '3774',
			'pool' => '3',
			'requerant' => 2,
			'dab' => 2150,
			'derogation' => 11.5
		],
		'6' => (object)[
			'parcelle' => '3983',
			'pool' => '2',
			'requerant' => 3,
			'dab' => 1298,
			'derogation' => 8.75
		],
		'9' => (object)[
			'parcelle' => '5697',
			'pool' => '6',
			'requerant' => 3,
			'dab' => 4562,
			'derogation' => 3
		],
		'20' => (object)[
			'parcelle' => '5697',
			'pool' => '8',
			'requerant' => 2,
			'dab' => 500,
			'derogation' => 0
		],
		'21' => (object)[
			'parcelle' => '5697',
			'pool' => '8',
			'requerant' => 3,
			'dab' => 450,
			'derogation' => 0
		],
		'10' => (object)[
			'parcelle' => '5697',
			'pool' => '4',
			'requerant' => 3,
			'dab' => 3470,
			'derogation' => 15.5
		],
	));
	}

	static function getCanceled() {
		return self::initRequeteArray(array(
		'3' => (object)[
			'parcelle' => '3774',
			'pool' => '5',
			'requerant' => 6
		],
		'17' => (object)[
			'parcelle' => '5687',
			'pool' => '2',
			'requerant' => 5
		],
		'4' => (object)[
			'parcelle' => '5687',
			'pool' => '6',
			'requerant' => 6
		],
		'5' => (object)[
			'parcelle' => '5697',
			'pool' => '7',
			'requerant' => 4
		],
		'7' => (object)[
			'parcelle' => '5697',
			'pool' => '8',
			'requerant' => 5
		],
		'16' => (object)[
			'parcelle' => '5697',
			'pool' => '3',
			'requerant' => 6
		],
	));
	}

	static function initRequeteArray($requetes) {
		foreach ($requetes as $key => $requete) {
			$requete->parcelle = Parcelles::get($requete->parcelle);
			$requete->pool = Pools::get($requete->pool);
			$requete->requerant = Requerants::get($requete->requerant);
		}
		return $requetes;
	}

	static function get($id) {
		$list = self::getAll();
		foreach ($list as $elementId => $requete) {
			if ($elementId == $id) {
				return $requete;
			}
		}

		return false;
	}

	static function getValidatedByRequerant($idRequerant) {
		$requests = self::getValidated();

		return self::filterByRequerant($requests, $idRequerant);
	}

	static function getCanceledByRequerant($idRequerant) {
		$requests = self::getCanceled();

		return self::filterByRequerant($requests, $idRequerant);
	}

	static function getOnGoingByRequerant($idRequerant) {
		$requests = self::getOnGoing();

		return self::filterByRequerant($requests, $idRequerant);
	}

	static function filterByRequerant($requests, $idRequerant) {
		$filtereds = [];

		foreach ($requests as $elementId => $requete) {
			if ($requete->requerant->id == $idRequerant) {
				$filtereds[$elementId] = $requete;
			}
		}

		return $filtereds;
	}

	static function getInConflict($idRequete) {
		$inConflict = [];
		$currentRequete = self::get($idRequete);

		$list = self::getOnGoing();
		foreach ($list as $elementId => $requete) {
			if ($elementId == $idRequete) {
				continue;
			}
			if (($requete->parcelle->id == $currentRequete->parcelle->id)
				&& ($requete->pool->id == $currentRequete->pool->id)) {
				$inConflict[$elementId] = $requete;
			}
		}

		return $inConflict;
	}

	public function getValidatedSpecific($idParcelle, $idPool) {
		$validated = [];
		$currentRequete = self::get($idRequete);

		$list = self::getValidated();
		foreach ($list as $elementId => $requete) {
			if (($requete->parcelle->id == $idParcelle)
				&& ($requete->pool->id == $idPool)) {
				$validated[$elementId] = $requete;
			}
		}

		return $validated;
	}
}
