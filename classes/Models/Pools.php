<?php

namespace Models;

class Pools {
	static function getAll() {
		return [
			(object)[
				'batiments' => 'A1',
				'id' => 1
			],
			(object)[
				'batiments' => 'B1, B2',
				'id' => 2
			],
			(object)[
				'batiments' => 'B3',
				'id' => 3
			],
			(object)[
				'batiments' => 'C1',
				'id' => 4
			],
			(object)[
				'batiments' => 'C2',
				'id' => 5
			],
			(object)[
				'batiments' => 'D1, D2',
				'id' => 6
			],
			(object)[
				'batiments' => 'E1, E2, E3',
				'id' => 7
			],
			(object)[
				'batiments' => 'F1',
				'id' => 8
			],
			(object)[
				'batiments' => 'G1, G2, G3',
				'id' => 9
			]
		];
	}

	static function get($id) {
		$list = self::getAll();
		foreach ($list as $pool) {
			if ($pool->id == $id) {
				return $pool;
			}
		}

		return false;
	}
}
