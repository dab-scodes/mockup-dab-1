<?php

namespace Models;

class Requerants {
	static function getAll() {
		return [
			(object)[
				'id' => 1,
				'patronyme' => 'Marcel Steiner',
				'dab_restant' => 3238
			],
			(object)[
				'id' => 2,
				'patronyme' => 'Sophia Moser',
				'dab_restant' => 5467
			],
			(object)[
				'id' => 3,
				'patronyme' => 'Paul Brunner',
				'dab_restant' => 8212
			],
			(object)[
				'id' => 4,
				'patronyme' => 'Marion Rolland',
				'dab_restant' => 4642
			],
			(object)[
				'id' => 5,
				'patronyme' => 'Markus Schneider',
				'dab_restant' => 4247
			],
			(object)[
				'id' => 6,
				'patronyme' => 'Olivia Frei',
				'dab_restant' => 4289
			],
		];
	}

	static function get($id) {
		$list = self::getAll();
		foreach ($list as $pool) {
			if ($pool->id == $id) {
				return $pool;
			}
		}

		return false;
	}
}
