<?php

class Password {
	static $pdo;
	static $isInited = false;

	static function init($pdo) {
		self::$pdo = $pdo;
		self::$isInited = true;
	}

	static function reinitLogin() {
		$newString = self::random_str(12);
		if(!self::replaceLogin($newString)) {
			return false;
		}

		return $newString;
	}

	static function sendPasswordEmail($password) {
		$to      = EMAIL_TO;
		$subject = 'Nouveau mot de passe';
		$message = 'Bonjour,<br>'
			.'<br/>Voici votre nouveau mot de passe:'
			.'<ul><li>'.$password.'</li></ul>'
		;
		$headers = 'From: ' .EMAIL_FROM. "\r\n";
		$headers .= 'Reply-To: ' .EMAIL_FROM. "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

		$test = mail($to, $subject, $message, $headers);
		return $test;
	}

	static function test($password) {
		return password_verify($password, self::getLogin());
	}

	static function setCookie() {
		return setcookie(COOKIE_NAME, md5(self::getLogin()), time()+60*60*24*365, '/', COOKIE_ONLY_HTTPS);
	}

	static function isLogged() {
		if (!isset($_COOKIE[COOKIE_NAME])) {
			return false;
		}
		return $_COOKIE[COOKIE_NAME] == md5(self::getLogin());
	}

	protected function getLogin() {
		$sql = "SELECT value FROM settings WHERE `key` = ?";
		$sql = self::$pdo->prepare($sql);
		$sql->execute(['password']);
		$row  = $sql->fetch();
		$password = isset($row['value']) ? $row['value'] : false;

		return $password;
	}

	protected function replaceLogin($password) {
		$encryptedPwd = password_hash($password, PASSWORD_DEFAULT);

		$sql = "DELETE FROM settings WHERE `key` = ?";
		self::$pdo->prepare($sql)->execute(['password']);

		$sql = "INSERT INTO `settings` (`key`, `value`) VALUES (?, ?)";
		self::$pdo->prepare($sql)->execute(['password', $encryptedPwd]);

		return true;
	}

	protected function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
	    $str = '';
	    $max = mb_strlen($keyspace, '8bit') - 1;
	    if ($max < 1) {
	        throw new Exception('$keyspace must be at least two characters long');
	    }
	    for ($i = 0; $i < $length; ++$i) {
	        $str .= $keyspace[random_int(0, $max)];
	    }
	    return $str;
	}
}
