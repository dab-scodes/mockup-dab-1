
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `image` text,
  `content` text,
  `title` varchar(255) DEFAULT NULL,
  `link_title` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `external` tinyint(4) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`),
  CONSTRAINT `block_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `block` WRITE;
/*!40000 ALTER TABLE `block` DISABLE KEYS */;
/*!40000 ALTER TABLE `block` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `name` text NOT NULL,
  `description` text,
  `sort` int(11) DEFAULT '0',
  `fa_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (15,'accueil','Accueil','<p>SÃ©lÃ©ctionnez ce que vous voulez faire dans le menu de gauche</p>',1,'home'),(16,'dab','Les droits Ã  bÃ¢tir','',2,'table'),(17,'requerants','RequÃ©rants','<p>Permet de crÃ©er un compte requÃ©rant</p>',3,'users'),(18,'contrats','Contrats',NULL,4,'handshake-o'),(19,'requetes','RequÃªtes',NULL,5,'question-circle'),(20,'enquetes','EnquÃªtes',NULL,6,'search'),(21,'consommation','Tableau de consommation ','',7,'check'),(22,'plq','Changer de PLQ','',0,'refresh');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `slug` varchar(255) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `section_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (104,16,'Les pools de bÃ¢timents','<p>CrÃ©er, modifier, supprimer les pools de bÃ¢timents</p>','batiments',1),(105,16,'Les parcelles','<p>CrÃ©er, modifier, supprimer des parcelles</p>','parcelles',0),(106,16,'Ã‰dition du Tableau','<p>Ã‰diter de le tableau de rÃ©partition des droits Ã  bÃ¢tir</p>','tableau',2),(108,18,'CrÃ©er un contrat','<p>Permet Ã  un requÃ©rant d\'Ã©tablir un contrat \"Parcelle - Pool de BÃ¢timents - RequÃ©rant\"</p>','creer',0),(110,19,'DÃ©poser une requÃªte','<p>Permet Ã  un requÃ©rant de dÃ©poser une requÃªte de construction d\'un pool de bÃ¢timent</p>','deposer',0),(111,19,'RequÃªtes en cours','<p>Permet de refuser ou de valider une requÃªte telle quelle et d\'y accorder une dÃ©rogation si besoin</p>','valider',1),(113,20,'EnquÃªte Ã  coder','<p>Permet de dÃ©ployer une enquÃªte Ã  coder</p>','a-coder',3),(114,17,'CrÃ©er un compte requÃ©rant','<p>Permet de crÃ©er un compte requÃ©rant </p>','creer',0),(115,16,'Liste des tableau Ã  valider','','liste',3),(116,16,'Tableau de rÃ©partition validÃ©','','valide',4),(117,20,'Total dÃ©rogation / Parcelle','<p>Le total des dÃ©rogations par parcelle</p>','derogation_parcelle',0),(118,20,'Total dÃ©rogation / RequÃ©rant','<p>Le total de dÃ©rogation par requÃ©rant</p>','derogation_requerant',1);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;
DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `key` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES ('password','$2y$10$jhbMETCufIyghjrp0qfSC.0tfXoY2PmOwcXkSkfU26ypdX6qoIJRa');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

